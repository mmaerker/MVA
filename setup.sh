#!/usr/bin/env bash

VERSION='0.0'
############################################################
# Help                                                     #
############################################################
Help()
{
   # Display Help
   echo "Setup script for NNTrainer package. (ver. $VERSION)"
   echo
   Usage
}

Usage()
{
   # Display Usage
   echo "Syntax: source setup.sh [-e|f|h|V|r]"
   echo "options:"
   echo "  -e     Specify the output of the python virtual environment (default is ~/Envs/)."
   echo "  -f     Force re-install. Allows to reset python env path."
   echo "  -h     Print this Help."
   echo "  -V     Print software version and exit."
   echo "  -r     Path to local ROOT installation (executes <PathToROOT/bin/thisroot.sh>)"
   echo
}

############################################################
# Function definitions                                     #
############################################################

setup_ATLAS(){
    ##======= Setup ATLAS
    # test if setupATLAS is defined, if not define it
    alias setupATLAS 2> /dev/null > /dev/null || (export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase && alias setupATLAS='source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh')
    setupATLAS -q
    lsetup git -q
}

save_envs(){
    envVars='LOCAL_ENV_DIR'
    if [ ! -z "$LOCAL_ROOT_PATH" ]; then
        envVars+=' LOCAL_ROOT_PATH'
    fi
    echo "">.setup_env
    for envVar in $envVars; do
        echo "export ${envVar}=${!envVar}">>.setup_env
    done
}

############################################################
# Main setup program                                       #
############################################################
OPTIND=1 # workaround if you want to source the shell script
while getopts ":hVfe::r:" option; do
    case $option in
        h) # display help
            Help
            return || exit;;
        V) # display version
            echo $VERSION
            return || exit;;
        e) # configure python environment
            if [ ! -d $OPTARG ]; then
                echo "$OPTARG is not a valid directory"
                return || exit
            fi
            export LOCAL_ENV_DIR=${OPTARG}/
            ;;
        f) # force re-install
            FORCE_REINSTALL=true
            ;;
        r) # force re-install
            LOCAL_ROOT_PATH=${OPTARG}/
            ;;
        ?) # for nonsensical input print help 
            echo "Unknown parameter! Usage:"
            Usage
            return || exit;;
    esac
done

##======= set configuration environment
export LOCAL_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
if [ -f "${LOCAL_DIR}/.setup_env" ] && [ -z "$FORCE_REINSTALL" ]; then
    echo "Setup from previous installation"
    source .setup_env
else
    echo "Setup new installation"
    if [ -z "$LOCAL_ENV_DIR" ]; then
        # set default python environment dir
        export LOCAL_ENV_DIR=${HOME}/Envs/
    fi
fi


##==== list of modules to install or load
PIP_PACKAGES=''
MODULE_LOADS=''
ENV_ADD_NAME=''

##======= fetch setup relevant variables
host=$(hostname)
ARCH=$(uname -m)


##======= Go through configuration setups
##======= [NOTE] Here is where you should add your config script if you plan to add one
##======= [NOTE] for your computing node
if [[ $host =~ 'taurus' ]]; then
    if [[ $ARCH =~ 'x86_64' ]]; then
        source $LOCAL_DIR/setupScripts/setup_taurus_x86_64.sh
    fi
    
    if [[ $ARCH =~ 'ppc64le' ]]; then
        source $LOCAL_DIR/setupScripts/setup_taurus_ppc64le.sh
    fi
    if [[ $host =~ 'taurusi80' ]]; then
        source $LOCAL_DIR/setupScripts/setup_taurus_alpha.sh
    fi

else
    if [[ $ARCH =~ 'x86_64' ]]; then
        source $LOCAL_DIR/setupScripts/setup_generic_x86_64.sh
    else
        echo "ERROR: cant handle architecture ${ARCH}!"
        echo "Please configure a setup in the setupScripts folder for your computing node!"
        echo "Detected computing node parameters are:"
        echo "   hostname:     $host"
        echo "   architecture: $ARCH"
    fi
fi

##======= Load scheduled modules
if [ -n "${MODULE_LOADS}" ]; then
    echo "Load requested system modules"
    for module in $MODULE_LOADS; do
        module load $module -q
    done
fi

##======= Setup python virtual environment  
##======= this is necessary to separate compiled source code for different architectures
ENV_NAME=MVA_${ARCH}
if [ ! -z "$ENV_ADD_NAME" ]; then
    ENV_NAME=${ENV_NAME}_${ENV_ADD_NAME}
fi
echo "Setting up virtual python environment"
echo "    Virtual environment name is: ($ENV_NAME)"
echo "Python environments will be installed here:"
echo "    $LOCAL_ENV_DIR"

mkdir -p ${LOCAL_ENV_DIR}/${ENV_NAME}
python3 -m venv --system-site-packages "${LOCAL_ENV_DIR}/${ENV_NAME}"
source "${LOCAL_ENV_DIR}/${ENV_NAME}/bin/activate"

##======= install local packages
if [ -n "${PIP_PACKAGES}" ]; then
    echo "Install requested python3 packages"
    for pkg in $PIP_PACKAGES; do
        echo "   Installing: $pkg"
        python3 -m pip install $pkg -q       
    done
fi

##======= set requested ROOT environment
if [ -z "$LOCAL_ROOT_PATH" ]; then
    echo "LOCAL_ROOT_PATH is not set, you may want to specify your local ROOT installation with -r"
else
    echo "Setup local ROOT installation"
    source $LOCAL_ROOT_PATH/bin/thisroot.sh
fi


##======= install scripts
export PYTHONPATH=${LOCAL_DIR}:${PYTHONPATH}
alias NNAplicator="python ${LOCAL_DIR}/python/NNApplicator"
alias NNTrainer="python ${LOCAL_DIR}/python/NNTrainer"
alias NNPlotSummary="python ${LOCAL_DIR}/python/PlotSummary"


save_envs
