from core import dataset
from core.sample import Sample
from core.dataset import RecurrentDataset, Dataset
from core.labels import RecurrentClassLabels
from core.weights import RecurrentClassBalance, RecurrentVarBalance
from core.utils.VarConfigExtractor import variables_to_dict

import keras
from keras.models import Sequential, Model
from keras.layers import Dense, Activation, LSTM, Masking, Input, Conv1D, Concatenate, Add, Multiply
from keras.layers import TimeDistributed, Bidirectional, BatchNormalization
from keras.callbacks import ReduceLROnPlateau, ModelCheckpoint
from keras.optimizers import Adam

from keras.utils import plot_model

def defineSamples(ConfHandler):

    TauSample = Sample(name = "TauSample",
                   proc = "Tau",
                   tree = "CollectionTree",
                   sub_dir = "")

    JetSample = Sample(name = "JetSample",
                   proc = "Jet",
                   tree = "CollectionTree",
                   sub_dir = "")

    ConfHandler.addAndConfigSample(TauSample)
    ConfHandler.addAndConfigSample(JetSample)

    TausDS= RecurrentDataset("TausDS")
    JetDS= RecurrentDataset("JetsDS")
    ConfHandler.addAndConfigDataset(TausDS)
    ConfHandler.addAndConfigDataset(JetDS)
    
    CombinedDS = RecurrentDataset("CombinedDS")
    CombinedDS.add_daughter(TausDS)
    CombinedDS.add_daughter(JetDS)


    ConfHandler.addAndConfigDataset(CombinedDS)

def defineLabels(ConfHandler):

    ## Offline: TT, CT, IT, FT
    #label = "({0} == 1)*0 + ({0} == 2)*1 + ({0} == 3)*2 + ({0} == 0 || {0} >= 4)*3".format("TruthType")
    ## Online: classify only TT and FT
    #label = "({0} == 1)*0 + ({0} != 1)*1".format(" TruthType")
    ## Online: classify only TT , CT and FT
    label = "({0} == 1)*0 + ({0} == 3)*1 + ({0} == 2 || {0} == 0 || {0} >= 4)*2".format("TruthType")
    LabelsDS= RecurrentClassLabels("LabelsCombined", label)(ConfHandler.getDataset("CombinedDS"))
    ConfHandler.addLabel(LabelsDS)

def defineWeights(ConfHandler):
    classWeight_TT1 = RecurrentClassBalance("classBalance_TT1", True, lable_weights={0: 1.})(ConfHandler.getLabel("LabelsCombined"))
    classWeight_TT3 = RecurrentClassBalance("classBalance_TT3", True, lable_weights={0: 3.})(ConfHandler.getLabel("LabelsCombined"))
    classWeight_TT5 = RecurrentClassBalance("classBalance_TT5", True, lable_weights={0: 5.})(ConfHandler.getLabel("LabelsCombined"))
    ptWeight =  RecurrentVarBalance("PtWeight", "jetSeedPt")(ConfHandler.getDataset("CombinedDS"))
    #ptWeight =  RecurrentVarBalance("PtWeight", "jetSeedPt")(ConfHandler.getDataset("CombinedDS"))

    ConfHandler.addWeight(classWeight_TT1)
    ConfHandler.addWeight(classWeight_TT3)
    ConfHandler.addWeight(classWeight_TT5)
    ConfHandler.addWeight(ptWeight)


def defineModel(ConfHandler):
    
    nLSTMnodes = ConfHandler.getNNConf("LSMTLayerSize")
    nLSTMlayers = ConfHandler.getNNConf("LSMTLayers")
    MergeType = ConfHandler.getNNConf("MergeType")
    DoBatchNorm = ConfHandler.getNNConf("DoBatchNorm")
    DoDropOut = ConfHandler.getNNConf("DoDropOut")

    inpShape = ConfHandler.getDataset("CombinedDS").get_train_data().shape[2]
    outShape = ConfHandler.getLabel("LabelsCombined").get_train_data().shape[2]

    inputs = Input(shape=(20    , inpShape))
    m = Masking(mask_value=0.)(inputs)
    td_1 = TimeDistributed(Dense(ConfHandler.getNNConf("DenseLayerSize1"), activation="relu"))(m)

    layPrev = td_1
    for iLayer in range(0, nLSTMlayers):
        dropout = 0.0
        if DoDropOut:
            dropout = 0.4
        layNow = Bidirectional( LSTM(nLSTMnodes, return_sequences=True, activation='relu', unroll=True, dropout=dropout, recurrent_dropout=dropout), merge_mode=ConfHandler.getNNConf("MergeType") )(layPrev)
        if DoBatchNorm:
            layNow = BatchNormalization()(layNow)
        layPrev = layNow

    td_2 = TimeDistributed(Dense(ConfHandler.getNNConf("DenseLayerSize2"), activation="relu"))(layPrev)
    td_3 = TimeDistributed(Dense(outShape, activation='softmax'))(td_2)
    
    model = Model(inputs=inputs, outputs=td_3)
    optimizer = Adam()
    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'], sample_weight_mode="temporal", weighted_metrics=['accuracy'])

    plot_model(model, to_file=ConfHandler.getModelOutput()+'vae_{}.png'.format(ConfHandler.kFoldConf), show_shapes=True)

    ConfHandler.setModel(model)

    inpLayer = inputs._keras_history[0]
    outLayer = td_3._keras_history[0]
    
    dInputs = {inpLayer : ConfHandler.getDataset("CombinedDS")}
    lOutputs = [outLayer]
    dVarConf = variables_to_dict(dInputs, lOutputs)
    ConfHandler.setVarDict(dVarConf)


def fitModel(ConfHandler):
    
    TTweightScale = ConfHandler.getFitConf("TT_weightScale")

    dataTrain = ConfHandler.getDataset("CombinedDS").get_train_data()
    dataVal   = ConfHandler.getDataset("CombinedDS").get_val_data()

    labTrain  = ConfHandler.getLabel("LabelsCombined").get_train_data()
    labVal    = ConfHandler.getLabel("LabelsCombined").get_val_data()

    clWTrain  = ConfHandler.getWeight("classBalance_"+TTweightScale).get_train_data()
    clWVal    = ConfHandler.getWeight("classBalance_TT1").get_val_data()

    ptWTrain  = ConfHandler.getWeight("PtWeight").get_train_data()
    ptWVal    = ConfHandler.getWeight("PtWeight").get_val_data()

    weightTrain = ptWTrain * clWTrain
    weightVal = ptWVal * clWVal


    epochs = ConfHandler.getFitConf("Epochs")
    batch_size = ConfHandler.getFitConf("BatchSize")
    model = ConfHandler.getModel()

    CBList = list()
    CBList.append(ModelCheckpoint(ConfHandler.getCheckPointName(), save_weights_only=True) )
    CBList.append(ReduceLROnPlateau(monitor='val_loss', factor=0.6, patience=5, min_lr=0.0001, verbose=1))
    history = model.fit(dataTrain, labTrain, sample_weight=weightTrain, epochs=epochs, batch_size=batch_size, validation_data=(dataVal, labVal, weightVal), callbacks=CBList, verbose=2)
    
    pred = model.predict(dataVal)

    ConfHandler.setFitHistory(history)

def applyModel(ConfHandler):

    dataTest = ConfHandler.getDataset("CombinedDS").get_test_data()
    model = ConfHandler.getModel()
    return model.predict(dataTest, batch_size=1000)