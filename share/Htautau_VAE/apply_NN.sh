#!/bin/sh
#SBATCH --cpus-per-task=1
#SBATCH --partition=sandy,haswell
#SBATCH --time=00:20:00
#SBATCH --mem-per-cpu=1700
#SBATCH --account=materie-09
NNCONF=/scratch/ws/s2205867-AUX/Trainings/Htautau/NNoutput_2020-02-07_14-28-39/worker/worker_HiddenLayers-3_LatentSize-2/
INPDIR=/scratch/ws/s2205867-AUX/finished_2019-08-24_HiggsResubmit_reduced/

OUTDIR=$1
OUTNAME=$2
DOFILE=$3


OUT=$OUTDIR/finished_NNApplication_$OUTNAME
if [ ! -z "$DOFILE" ]
then
    cd $MVAAREA
    source setup_taurus.sh
    python python/NNApplicator $NNCONF -i $INPDIR/fac_TAUS -o $OUT/fac_TAUS -p $DOFILE

else

    mkdir $OUT
    mkdir $OUT/data
    mkdir $OUT/fac_TAUS   
    
    cd $MVAAREA
    python python/NNApplicator $NNCONF -i $INPDIR/data_merged -o $OUT/data
    
#    PATTERNS=""
#    
#    for file in $INPDIR/fac_TAUS/*
#    do
#        PATTERNS+=" ${file##*/}"
#    done
#    
#    #for pattern in $PATTERNS
#    #do
#    #    #sbatch apply_NN.sh $OUTDIR $OUTNAME $pattern
#    #    python python/NNApplicator $NNCONF -i $INPDIR/fac_TAUS -o $OUT/fac_Taus -p pattern
#    #done
#    parallel -j5 nice -n 19 python python/NNApplicator $NNCONF -i $INPDIR/fac_TAUS -o $OUT/fac_TAUS -p {0} ::: $PATTERNS 
    cd -
fi