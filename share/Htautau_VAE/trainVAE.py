from core import dataset
from core.sample import Sample
from core.dataset import RecurrentDataset, Dataset
from core.labels import RecurrentClassLabels
from core.weights import RecurrentClassBalance, RecurrentVarBalance, DatasetBalance
from core.utils.VarConfigExtractor import variables_to_dict

import keras as keras
from keras.callbacks import Callback
from keras import backend as K
from keras.models import Model
from keras.layers import Dense, Lambda, Input
from keras.callbacks import ReduceLROnPlateau, ModelCheckpoint
from keras.optimizers import Adam
from keras.losses import mse, binary_crossentropy
from keras.utils import plot_model


def sampling(args):
    """Reparameterization trick by sampling from an isotropic unit Gaussian.

    # Arguments
        args (tensor): mean and log of variance of Q(z|X)

    # Returns
        z (tensor): sampled latent vector
    """

    z_mean, z_log_var = args
    batch = K.shape(z_mean)[0]
    dim = K.int_shape(z_mean)[1]
    # by default, random_normal has mean = 0 and std = 1.0
    epsilon = K.random_normal(shape=(batch, dim))
    return z_mean + K.exp(0.5 * z_log_var) * epsilon

def defineSamples(ConfHandler):
    #samples configured by JobConf.yml
    lMasses = ["200", "250", "300", "350", "400", "500", "600", "700", "800", "1000", "1200", "1500", "2000", "2500"]

    CombinedDS = Dataset("CombinedDS")
    
    for mass in lMasses:
        ConfHandler.addAndConfigSample( Sample(name = "ggH{}Sample".format(mass)) )
        ConfHandler.addAndConfigDataset( Dataset("H{}_DS".format(mass)) )
        CombinedDS.add_daughter(ConfHandler.getDataset("H{}_DS".format(mass)))
    
    ConfHandler.addAndConfigDataset(CombinedDS)

    BasePath = "/home/s2205867/MVA/MVA/"
    ConfHandler.addSample( Sample(name = "MultijetSample",
                                  tree = "STT_OS_CR_BVETO_tree_OS",
                                  proc = "Multijet",
                                  is_data   = True,
                                  file_path = "/scratch/ws/s2205867-AUX/finished_2019-08-24_HiggsResubmit_reduced/data_merged/",
                                  mergeconf_file = BasePath+"share/Htautau_VAE/HtautauMergConf.yml",
                                  mcweights_file = BasePath+"share/Htautau_VAE/MCWeights.yml" ))
    
    ConfHandler.addAndConfigDataset(Dataset("MultijetDS", is_spectator=True))
    #ConfHandler.addAndConfigSample( Sample(name = "TopSample") )
    #ConfHandler.addAndConfigDataset( Dataset("Top_DS") )
    

def defineLabels(ConfHandler):
    pass
    #label = "({0} == 1)*0 + ({0} == 2)*1 + ({0} == 3)*2 + ({0} == 0 || {0} >= 4)*3".format("TruthType")
    #LabelsDS= RecurrentClassLabels("LabelsCombined", label)(ConfHandler.getDataset("CombinedDS"))
    #ConfHandler.addLabel(LabelsDS)

def defineWeights(ConfHandler):
    DSBalance = DatasetBalance("DSbalance")(ConfHandler.getDataset("CombinedDS"))
    ConfHandler.addWeight(DSBalance)


def defineModel(ConfHandler):

    input_shape = ConfHandler.getDataset("CombinedDS").get_train_data().shape[1]

    hidden_dim = ConfHandler.getNNConf("HiddenSize")
    hidden_layers = ConfHandler.getNNConf("HiddenLayers")
    latent_dim = ConfHandler.getNNConf("LatentSize")

    lLayerSize = [int(hidden_dim/i) for i in range(1, hidden_layers+1) if hidden_dim/i > latent_dim*2]

    # VAE model = encoder + decoder
    # build encoder model
    inputs = Input(shape=(input_shape,), name='encoder_input')

    x_prev = inputs
    for layerSize in lLayerSize:
        x = Dense(layerSize, activation='relu')(x_prev)
        x_prev = x
        
    z_mean = Dense(latent_dim, name='z_mean')(x_prev)
    z_log_var = Dense(latent_dim, name='z_log_var')(x)
    
    # use reparameterization trick to push the sampling out as input
    # note that "output_shape" isn't necessary with the TensorFlow backend
    z = Lambda(sampling, output_shape=(latent_dim,), name='z_comb')([z_mean, z_log_var])    

    # build decoder model
    x_prev = z
    lLayerSize.reverse()
    for layerSize in lLayerSize:
        x = Dense(layerSize, activation='relu')(x_prev)
        x_prev = x
    
    outputs = Dense(input_shape, activation='linear', name="decoder_output")(x)
        
    # instantiate VAE model
    model = Model(inputs, outputs, name='vae_mlp')

    plot_model(model, to_file=ConfHandler.getModelOutput()+'vae_{}.png'.format(ConfHandler.kFoldConf), show_shapes=True)

    optimizer = Adam()

    reconstruction_loss = mse(inputs,outputs)
    reconstruction_loss *= input_shape
    kl_loss = 1 + z_log_var - K.square(z_mean) - K.exp(z_log_var)
    kl_loss = K.sum(kl_loss, axis=-1)
    kl_loss *= -0.5
    vae_loss = K.mean(reconstruction_loss + kl_loss)
    model.add_loss(vae_loss)
    model.compile(optimizer=optimizer, metrics=['accuracy'])

    ConfHandler.setModel(model)


def fitModel(ConfHandler):
    
    dataTrain = ConfHandler.getDataset("CombinedDS").get_train_data()
    dataVal   = ConfHandler.getDataset("CombinedDS").get_val_data()
    #dataTopVal   = ConfHandler.getDataset("Top_DS").get_val_data()

    weightTrain  = ConfHandler.getWeight("DSbalance").get_train_data()
    weightVal    = ConfHandler.getWeight("DSbalance").get_val_data()

    dataMultijet = ConfHandler.getDataset("MultijetDS").get_val_data()
    LossMSE = keras.losses.MeanSquaredError()

    epochs = ConfHandler.getFitConf("Epochs")
    batch_size = ConfHandler.getFitConf("BatchSize")
    model = ConfHandler.getModel()

    CBList = list()
    CBList.append(ModelCheckpoint(ConfHandler.getCheckPointName(), save_weights_only=True) )
    CBList.append(ReduceLROnPlateau(monitor='val_loss', factor=0.3, patience=3, min_lr=0.0001, verbose=1))
    CBList.append(LossLogger(LossMSE, dataVal, dataMultijet, model) )
    history = model.fit(dataTrain, sample_weight=weightTrain, epochs=epochs, batch_size=batch_size, validation_data=(dataVal, None, weightVal), callbacks=CBList)
    
    #LossMSE = keras.losses.MeanSquaredError(reduction='none')
    #LossMSE_red = keras.losses.MeanSquaredError()

    #print "=========================== Test on Signal val dataset: "
    #pred = model.predict(dataVal)
    #MSE = LossMSE(dataVal, pred)
    #print pred
    #print MSE
    #print LossMSE_red(dataVal, pred)
    #
    #print "=========================== Test on Top val dataset: "
    #pred = model.predict(dataTopVal)
    #MSE = LossMSE(dataTopVal, pred)
    #print pred
    #print MSE
    #print LossMSE_red(dataTopVal, pred)


    ConfHandler.setFitHistory(history)

def applyModel(ConfHandler):

    dataTest = ConfHandler.getDataset("CombinedDS").get_test_data()
    model = ConfHandler.getModel()

    LossMSE = keras.losses.MeanSquaredError(reduction='none')
    pred = model.predict(dataTest)

    MSE_tensor = LossMSE(dataTest, pred)
    return MSE_tensor.numpy()


class LossLogger(Callback):
    def __init__(self, loss, data_sig, data_bkg, model):
        self.loss = loss
        self.data_sig = data_sig
        self.data_bkg = data_bkg
        self.model = model        

    def on_epoch_end(self, epoch, logs=None):
        logs["loss_sig"] = float(self.loss(self.data_sig, self.model.predict(self.data_sig)))
        logs["loss_bkg"] = float(self.loss(self.data_bkg, self.model.predict(self.data_bkg)))