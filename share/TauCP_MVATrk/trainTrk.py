from core import dataset
from core.sample import Sample
from core.dataset import RecurrentDataset, Dataset
from core.labels import RecurrentClassLabels
from core.weights import RecurrentClassBalance, RecurrentVarBalance
from core.utils.VarConfigExtractor import variables_to_dict

import tensorflow.keras as keras
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.layers import Dense, Activation, LSTM, Masking, Input, Conv1D, Concatenate, Add, Multiply
from tensorflow.keras.layers import TimeDistributed, Bidirectional
from tensorflow.keras.callbacks import ReduceLROnPlateau, ModelCheckpoint
from tensorflow.keras.optimizers import Adam

def defineSamples(ConfHandler):

    TauSample = Sample(name = "TauSample",
                   proc = "Tau",
                   tree = "CollectionTree",
                   sub_dir = "")

    JetSample = Sample(name = "JetSample",
                   proc = "Jet",
                   tree = "CollectionTree",
                   sub_dir = "")

    ConfHandler.addAndConfigSample(TauSample)
    ConfHandler.addAndConfigSample(JetSample)

    TausDS= RecurrentDataset("TausDS")
    JetDS= RecurrentDataset("JetsDS")
    ConfHandler.addAndConfigDataset(TausDS)
    ConfHandler.addAndConfigDataset(JetDS)
    
    CombinedDS = RecurrentDataset("CombinedDS")
    CombinedDS.add_daughter(TausDS)
    CombinedDS.add_daughter(JetDS)


    ConfHandler.addAndConfigDataset(CombinedDS)

def defineLabels(ConfHandler):

    label = "({0} == 1)*0 + ({0} == 2)*1 + ({0} == 3)*2 + ({0} == 0 || {0} >= 4)*3".format("TruthType")
    LabelsDS= RecurrentClassLabels("LabelsCombined", label)(ConfHandler.getDataset("CombinedDS"))
    ConfHandler.addLabel(LabelsDS)

def defineWeights(ConfHandler):
    classWeight = RecurrentClassBalance("classBalance", True)(ConfHandler.getLabel("LabelsCombined"))
    ptWeight =  RecurrentVarBalance("PtWeight", "jetSeedPt")(ConfHandler.getDataset("CombinedDS"))

    ConfHandler.addWeight(classWeight)
    ConfHandler.addWeight(ptWeight)


def defineModel(ConfHandler):
    
    nLSTMnodes = ConfHandler.getNNConf("LSMTLayerSize")
    MergeType = ConfHandler.getNNConf("MergeType")

    inpShape = ConfHandler.getDataset("CombinedDS").get_train_data().shape[2]
    outShape = ConfHandler.getLabel("LabelsCombined").get_train_data().shape[2]

    inputs = Input(shape=(None, inpShape))
    m = Masking(mask_value=0.)(inputs)
    td_1 = TimeDistributed(Dense(ConfHandler.getNNConf("DenseLayerSize1"), activation="relu"))(m)

    lstm_1 = Bidirectional( LSTM(nLSTMnodes, return_sequences=True, activation='relu'), merge_mode=ConfHandler.getNNConf("MergeType") )(td_1)
    lstm_2 = Bidirectional( LSTM(nLSTMnodes, return_sequences=True, activation='relu'), merge_mode=ConfHandler.getNNConf("MergeType") )(lstm_1)
    lstm_3 = Bidirectional( LSTM(nLSTMnodes, return_sequences=True, activation='relu'), merge_mode=ConfHandler.getNNConf("MergeType") )(lstm_2)
    
    td_2 = TimeDistributed(Dense(ConfHandler.getNNConf("DenseLayerSize2"), activation="relu"))(lstm_3)
    td_3 = TimeDistributed(Dense(outShape, activation='softmax'))(td_2)
    
    model = Model(inputs=inputs, outputs=td_3)
    optimizer = Adam(learning_rate=0.005)
    model.compile(optimizer=optimizer, loss='categorical_crossentropy', metrics=['accuracy'], sample_weight_mode="temporal", )

    ConfHandler.setModel(model)

    inpLayer = inputs._keras_history[0]
    outLayer = td_3._keras_history[0]
    
    dInputs = {inpLayer : ConfHandler.getDataset("CombinedDS")}
    lOutputs = [outLayer]
    dVarConf = variables_to_dict(dInputs, lOutputs)
    ConfHandler.setVarDict(dVarConf)


def fitModel(ConfHandler):
    
    dataTrain = ConfHandler.getDataset("CombinedDS").get_train_data()
    dataVal   = ConfHandler.getDataset("CombinedDS").get_val_data()

    labTrain  = ConfHandler.getLabel("LabelsCombined").get_train_data()
    labVal    = ConfHandler.getLabel("LabelsCombined").get_val_data()

    ptWTrain  = ConfHandler.getWeight("classBalance").get_train_data()
    ptWVal    = ConfHandler.getWeight("classBalance").get_val_data()

    clWTrain  = ConfHandler.getWeight("PtWeight").get_train_data()
    clWVal    = ConfHandler.getWeight("PtWeight").get_val_data()

    weightTrain = ptWTrain * clWTrain
    weightVal = ptWVal * clWVal

    epochs = ConfHandler.getFitConf("Epochs")
    batch_size = ConfHandler.getFitConf("BatchSize")
    model = ConfHandler.getModel()

    CBList = list()
    CBList.append(ModelCheckpoint(ConfHandler.getCheckPointName(), save_weights_only=True) )
    CBList.append(ReduceLROnPlateau(monitor='val_loss', factor=0.3, patience=3, min_lr=0.0001, verbose=1))
    history = model.fit(dataTrain, labTrain, sample_weight=weightTrain, epochs=epochs, batch_size=batch_size, validation_data=(dataVal, labVal, weightVal), callbacks=CBList)
    
    ConfHandler.setFitHistory(history)

def applyModel(ConfHandler):

    dataTest = ConfHandler.getDataset("CombinedDS").get_test_data()
    model = ConfHandler.getModel()
    return model.predict(dataTest, batch_size=1000)