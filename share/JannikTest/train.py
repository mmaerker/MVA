from core import dataset
from core.sample import Sample
from core.dataset import Dataset
from core.labels import DatasetClassLables
from core.weights import DatasetBalance

import tensorflow.keras as keras
from tensorflow.keras.callbacks import Callback
from tensorflow.keras import backend as K
from tensorflow.keras.models import Model
from tensorflow.keras.layers import Dense, Lambda, Input
from tensorflow.keras.callbacks import ReduceLROnPlateau, ModelCheckpoint
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import mse, binary_crossentropy
from tensorflow.keras.utils import plot_model

import numpy as np

def defineSamples(ConfHandler):
    event_trigger_sel =     "(e7mu24 == 1)   *(ele_pt1  < 18000)*(muon_pt1 > 25000)*(ele_pt1 > 8000) + "\
                            "(e24mu8 == 1)   *(muon_pt1 < 15000)*(muon_pt1 > 10000) *(ele_pt1 > 27000) +"\
                            "(e17mu14 == 1)  *(muon_pt1 > 15000)*(ele_pt1  > 18000)"

    event_kin_sel = "(dR_lep_lep < 1.0)*(mTtot < 65000.)"

    muon_iso = "Tight"
    ele_iso  = "Tight"

    muon_id = 2
    ele_id  = 2

    event_isoLep1_sel = "(abs(leadlep_type) == 11)*(leadlep_pass{}Iso == 1) + " \
                        "(abs(subleadlep_type) == 11)*(subleadlep_pass{}Iso == 1)".format(ele_iso, ele_iso)
    event_idLep1_sel  = "(abs(leadlep_type) == 11)*(leadlep_id_quali < {}) + " \
                        "(abs(subleadlep_type) == 11)*(subleadlep_id_quali < {})".format(ele_id, ele_id)
    
    event_isoLep2_sel = "(abs(leadlep_type) == 13)*(leadlep_pass{}Iso == 1) + " \
                        "(abs(subleadlep_type) == 13)*(subleadlep_pass{}Iso == 1)".format(muon_iso, muon_iso)
    event_idLep2_sel  = "(abs(leadlep_type) == 13)*(leadlep_id_quali < {}) + " \
                        "(abs(subleadlep_type) == 13)*(subleadlep_id_quali < {})".format(muon_id, muon_id)

    cut_list = [event_trigger_sel, event_kin_sel, event_isoLep1_sel, event_idLep1_sel, event_isoLep2_sel, event_idLep2_sel]
    total_sel = "*".join(["({})".format(cut) for cut in  cut_list])


    #samples configured by JobConf.yml
    # lMasses = ["40", "50", "60", "70"]
    lMasses = ["60"]

    SignalDS = Dataset("SignalDS")
    
    for mass in lMasses:
        ConfHandler.addAndConfigSample( Sample(name = "ggA{}Sample".format(mass)) )
        ConfHandler.addAndConfigDataset( Dataset("A{}_DS".format(mass), sel=total_sel) )
        SignalDS.add_daughter(ConfHandler.getDataset("A{}_DS".format(mass)))
    
    ConfHandler.addAndConfigDataset(SignalDS)

    ConfHandler.addAndConfigSample( Sample(name="DYtautauSample") )
    DYtautauDS = Dataset("DYtautauDS", sel=total_sel)
    ConfHandler.addAndConfigDataset( DYtautauDS )

    ConfHandler.addAndConfigSample( Sample(name="TopSample") )
    TopDS = Dataset("TopDS", sel=total_sel)
    ConfHandler.addAndConfigDataset( TopDS )

    BackgroundDS = Dataset("BackgroundDS")
    BackgroundDS.add_daughter(DYtautauDS)
    BackgroundDS.add_daughter(TopDS)
    ConfHandler.addAndConfigDataset(BackgroundDS)

    CombinedDS = Dataset("CombinedDS")
    CombinedDS.add_daughter(BackgroundDS)
    CombinedDS.add_daughter(SignalDS)
    ConfHandler.addAndConfigDataset(CombinedDS)


    #ConfHandler.addAndConfigSample( Sample(name = "TopSample") )
    #ConfHandler.addAndConfigDataset( Dataset("Top_DS") )
    

def defineLabels(ConfHandler):
    LabelsDS = DatasetClassLables(name="ClassLabels", dataset_keys=["SignalDS", "BackgroundDS"], do_binary=True)(ConfHandler.getDataset("CombinedDS"))
    ConfHandler.addLabel(LabelsDS)

def defineWeights(ConfHandler):
    DSBalance = DatasetBalance("DSbalance", ds_keys_weights={"TopDS" : 1., "DYtautauDS" : 1.})(ConfHandler.getDataset("CombinedDS"))
    ConfHandler.addWeight(DSBalance)
    pass


def defineModel(ConfHandler):
    ## First get input shape
    input_shape = ConfHandler.getDataset("CombinedDS").get_train_data().shape[1]

    hidden_dim = ConfHandler.getNNConf("HiddenSize")
    hidden_layers = ConfHandler.getNNConf("HiddenLayers")

    # define model layout
    inputs = Input(shape=(input_shape,))

    x_prev = inputs
    for i in range(hidden_layers):
        x = Dense(hidden_dim, activation='relu')(x_prev)
        x_prev = x
    
    outputs = Dense(1, activation='sigmoid')(x_prev)
        
    # buuild model
    model = Model(inputs, outputs, name='mlp')

    plot_model(model, to_file=ConfHandler.getModelOutput()+'model_{}.png'.format(ConfHandler.kFoldConf), show_shapes=True)

    model.compile(optimizer="adam", loss="binary_crossentropy", metrics=['accuracy'], weighted_metrics=['accuracy'])

    ConfHandler.setModel(model)
    pass


def fitModel(ConfHandler):

    CombDataTrain = ConfHandler.getDataset("CombinedDS").get_train_data()
    CombDataVal   = ConfHandler.getDataset("CombinedDS").get_val_data()

    # print(dataTrain.shape)
    # print(DYDataTrain.shape)
    # print(TopDataTrain.shape)
    print(CombDataTrain.shape)

    labelsTrain = ConfHandler.getLabel("ClassLabels").get_train_data()
    labelsVal   = ConfHandler.getLabel("ClassLabels").get_val_data()
    print(labelsTrain.shape)

    weightTrain  = ConfHandler.getWeight("DSbalance").get_train_data()
    weightVal    = ConfHandler.getWeight("DSbalance").get_val_data()
    # print(weightTrain)
    
    epochs = ConfHandler.getFitConf("Epochs")
    batch_size = ConfHandler.getFitConf("BatchSize")
    model = ConfHandler.getModel()

    CBList = list()
    # CBList.append(ModelCheckpoint(ConfHandler.getCheckPointName(), save_weights_only=True) )
    # CBList.append(ReduceLROnPlateau(monitor='val_loss', factor=0.3, patience=3, min_lr=0.0001, verbose=1))
    history = model.fit(CombDataTrain, labelsTrain, sample_weight=weightTrain, batch_size=batch_size,
                                  epochs=epochs, 
                                  validation_data=(CombDataVal, labelsVal, weightVal), 
                                  callbacks=CBList,
                                  verbose=2)

    ConfHandler.setFitHistory(history)
    pass

def applyModel(ConfHandler):
    dataTest = ConfHandler.getDataset("CombinedDS").get_test_data()
    model = ConfHandler.getModel()
    dOutPred = {}
    pred = model.predict(dataTest)
    dOutPred["score"] = pred

    return dOutPred