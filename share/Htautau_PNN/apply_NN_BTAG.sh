#!/bin/sh
#SBATCH --cpus-per-task=1
#SBATCH --partition=sandy,haswell
#SBATCH --time=00:20:00
#SBATCH --mem-per-cpu=1700
#SBATCH --account=materie-09

#BTAG conf
NNCONF=/scratch/ws/s2205867-AUX/Trainings/Htautau/BTag_MJ_Top_PNN/NNoutput_2020-02-25_21-16-23/worker/worker_HiddenLayers-3_HiddenSize-20/
INPDIR=/scratch/ws/s2205867-AUX/Higg_NTuples_v15_newVarsForRNN/finished_2020-02-13_Higgs_reduced2/

OUTDIR=$1
OUTNAME=$2

TREES="STT_OS_SR_BTAG_tree_OS STT_OS_CR_BTAG_tree_OS STT_SS_SR_BTAG_tree_SS STT_SS_CR_BTAG_tree_SS"


OUT=$OUTDIR/finished_NNApplication_$OUTNAME

mkdir $OUT
mkdir $OUT/data
mkdir $OUT/fac_TAUS   
    
cd $MVAAREA
python python/NNApplicator $NNCONF -i $INPDIR/data_merged -o $OUT/data --trees ${TREES}
    
PATTERNS=""
    
for file in $INPDIR/fac_TAUS/*
do
    fName="${file##*/}"
    if [ ! -f "$OUT/fac_TAUS/$fName" ]; then
        PATTERNS+=" ${file##*/}"
    fi
done

echo $PATTERNS
    
#for pattern in $PATTERNS
#do
#    #sbatch apply_NN.sh $OUTDIR $OUTNAME $pattern
#    python python/NNApplicator $NNCONF -i $INPDIR/fac_TAUS -o $OUT/fac_Taus -p pattern
#done
parallel -j8 nice -n 19 python python/NNApplicator $NNCONF -i $INPDIR/fac_TAUS -o $OUT/fac_TAUS -p {0} --trees ${TREES} ::: $PATTERNS 
cd -
