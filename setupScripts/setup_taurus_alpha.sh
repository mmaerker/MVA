echo "Installing software for ZIH taurus Alpha Centauri node"
PIP_PACKAGES='six matplotlib pyyaml atlasify bitarray ruamel.yaml logger root_numpy pydot graphviz'
MODULE_LOADS='modenv/hiera GCC/10.2.0 CUDA/11.1.1 OpenMPI/4.0.5 TensorFlow/2.4.1 CMake X11'
LOCAL_ROOT_PATH=/beegfs/global0/ws/s2205867-FastData/root_build_alpha/
ENV_ADD_NAME='alpha'