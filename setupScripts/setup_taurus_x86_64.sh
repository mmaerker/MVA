echo "Installing software for ZIH taurus x86_64 node"
PIP_PACKAGES='six matplotlib pyyaml atlasify bitarray ruamel.yaml logger root_numpy pydot graphviz'
#ATTENTION: Order matters!
MODULE_LOADS='TensorFlow/2.3.1-fosscuda-2019b-Python-3.7.4 Boost/1.74.0-GCC-10.2.0 CMake'
LOCAL_ROOT_PATH=/beegfs/global0/ws/s2205867-FastData/root_build/
ENV_ADD_NAME='taurus'