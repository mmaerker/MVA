import argparse, json, glob, ROOT
from core.factory import Worker
import numpy as np
import root_numpy
import array
from core.utils.logger import log

def getWorkerData(workerConf):
    dWorkerData = {}
    with open(workerConf, "r") as json_file:
        dWorkerData = json.load(json_file) 
    return dWorkerData

def createWorkers(lWorkerConfFiles, epoch):
    lWorkers = []
    for workerConf in lWorkerConfFiles:
        dWorkerData = getWorkerData(workerConf)
        lWorkers.append(Worker(**dWorkerData))
        lWorkers[-1].reloadModel(epoch)
    return lWorkers

def checkOutputDim(outArr):
    # this function checks the dimention of the prediction output
    # allows shape (#Events) and (#Events, 1) and rectifies them to (#Events)
    # throws error if dimention is differnt
    if (len(outArr.shape) == 1): return outArr
    if (len(outArr.shape) == 2):
        if outArr.shape[1] == 1:
            return np.stack(outArr, axis=1)[0]
    log().error("Output of prediction has shape {}. Only shape of (#Events) or (#Events, 1) are allowed".format(outArr.shape))
    return None

def runSample(fileNamePath, tTree, lWorkers, merge_mode="ave"):
    assert merge_mode in ["ave", "concat"], "Cannot combine NN output with {}. Pls use obe of ['ave', 'concat']".format(merge_mode)
    
    dOutArray = {}
    aDataArr = np.zeros(tTree.GetEntries())
    for wNum, worker in enumerate(lWorkers):
        fileName = fileNamePath.split("/")[-1]
        arrPredict = worker.getPrediction(fileName, tTree)

        if type(arrPredict) == type(None): continue
        
        if not isinstance(arrPredict, dict):
            arrPredict = {"NN_score" : arrPredict}

        for branchName, arr in arrPredict.items():
            if not branchName in dOutArray:
                dOutArray[branchName] = np.zeros(tTree.GetEntries())
            
            if worker.isUsedInTraining(fileNamePath) or merge_mode == "concat":
                predIndices = worker.getPredictionIndices()
                dOutArray[branchName][predIndices] = checkOutputDim(arr)
            else:
                dOutArray[branchName] += checkOutputDim(arr)
                if(wNum == (len(lWorkers)-1)):
                    dOutArray[branchName] *= 1.0/float(len(lWorkers))

    return dOutArray

def getFileFolders(tFile):
    lFolders = []
    for key in tFile.GetListOfKeys():
        if "TDirectory" in key.GetClassName(): lFolders.append(key.GetName())
    return lFolders

def copyAux(tFileInp, tFileOut, currentDir=""):
    inpSubDir = None
    outSubDir = None
    if currentDir:
        inpSubDir = tFileInp.GetDirectory(currentDir)
        tFileOut.mkdir(currentDir)
        tFileOut.cd()
        tFileOut.cd(currentDir)
        outSubDir = tFileOut.GetDirectory(currentDir)
    else:
        inpSubDir = tFileInp
        outSubDir = tFileOut
        tFileInp.cd()
        tFileOut.cd()

    for key in inpSubDir.GetListOfKeys():
        if "TH1D" in key.GetClassName():
            copyObj = key.ReadObj().Clone()
            copyObj.SetDirectory(outSubDir)
            copyObj.Write()
        elif "TDirectory" in key.GetClassName():
            subDir = currentDir+key.GetName()+"/"
            tFileInp.cd()
            tFileOut.cd()
            copyAux(tFileInp, tFileOut, subDir)
    
    tFileInp.cd()
    tFileOut.cd()


def main(args):
    if not args.workerDir.endswith("/"):
        args.workerDir += "/"
    if not args.outputFolder.endswith("/"):
        args.outputFolder += "/"

    lWorkerConfFiles = glob.glob(args.workerDir+"worker_conf*.json")
    lWorkers = createWorkers(lWorkerConfFiles, args.epoch)

    tFile = ROOT.TFile.Open(args.sample,"READ")
    outFileName = args.outputFolder + args.sample.split("/")[-1]
    tOutFile = ROOT.TFile.Open(outFileName, "RECREATE")
    copyTree = None
    
    lFolders = getFileFolders(tFile)
    copyAux(tFile, tOutFile)
    for subFolder in lFolders:
        tOutFile.mkdir(subFolder)
        tOutFile.cd(subFolder)
        for treeName in args.trees:
            tTree = tFile.Get("{}/{}".format(subFolder,treeName))
            # only run if tree is not empty
            
            dOutArray = {}
            if tTree.GetEntries() > 0:
                dOutArray = runSample(args.sample, tTree, lWorkers)
            copyTree = tTree.CopyTree("1.")
            lBranches = []
            lSubArrays = []
            lOutArrays = []
            for branchName, outArray in dOutArray.items():
                lSubArrays.append(array.array("d", [0]))
                lBranches.append(copyTree.Branch(branchName, lSubArrays[-1], branchName+'/D'))
                lOutArrays.append(outArray)
            for i in range(0, tTree.GetEntries()):
                copyTree.GetEntry(i)
                for j in range(0, len(lBranches)):
                    lSubArrays[j][0] = lOutArrays[j][i]
                    lBranches[j].Fill()
            copyTree.Write()
        tOutFile.cd()
    tOutFile.Close()
    tFile.Close()



if __name__ == "__main__":

    parser = argparse.ArgumentParser()

    parser.add_argument("workerDir", type=str, help="Directory where trained workers can be found")
    parser.add_argument("-s", "--sample", type=str, default="./", help="Folder with flat nTuples")
    parser.add_argument("-o", "--outputFolder", type=str, default="./", help="Folder to dump results, not needed if runMode is update")
    parser.add_argument("-e", "--epoch", type=int, default=0, help="epoch to apply, default: use last (alias 0)")

    parser.add_argument("-r", "--runMode", type=str, default="fullCopy", help="Runmode, coode from [fullCopy, shallowCopy, update]")
    parser.add_argument("-t", "--trees", nargs="+", type=str, default=["STT_OS_SR_BVETO_tree_OS", "STT_OS_CR_BVETO_tree_OS", "STT_SS_SR_BVETO_tree_SS", "STT_SS_CR_BVETO_tree_SS"], help="")

    args = parser.parse_args()

    main(args)